import { initGlobalOptions } from "~/components/Charts/config.js";
import './roundedCornersExtension'
export default {
  mounted() {
    initGlobalOptions();
  }
}
